package ceballos.adrian.tryagain;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class FriendsList extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        FriendsFragment friendsFragment = new FriendsFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container,friendsFragment).commit();
    }
}
