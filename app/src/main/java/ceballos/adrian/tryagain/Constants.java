package ceballos.adrian.tryagain;

/**
 * Created by Student on 6/1/2016.
 */
public class Constants {
    public static final String ACTION_ADD_FRIEND = "ceballos.adrian.tryagain.ADD_FRIEND";
    public static final String ACTION_SEND_FRIEND_REQUEST = "ceballos.adrian.tryagain.SEND_FRIEND_REQUEST";
    public static final String BROADCAST_ADD_FRIEND_SUCCESS = "ceballos.adrian.tryagain.ADD_FRIEND_SUCCESS";
    public static final String BROADCAST_ADD_FRIEND_FAILURE = "ceballos.adrian.tryagain.ADD_FRIEND_FAILURE";
    public static final String BROADCAST_FRIEND_REQUEST_SUCCESS = "ceballos.adrian.tryagain.FRIEND_REQUEST_SUCCESS";
    public static final String BROADCAST_FRIEND_REQUEST_FAILURE = "ceballos.adrian.tryagain.FRIEND_REQUEST_FAILURE";

}
