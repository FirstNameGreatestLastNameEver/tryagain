package ceballos.adrian.tryagain;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.backendless.Backendless;

public class MainActivity extends AppCompatActivity {
    public final static String APP_ID = "B51A683A-6DA1-CDE0-FFB2-59DD25A69300";
    public final static String SECRET_KEY = "BBC6A52E-A73B-8169-FF9E-F163055E2F00";
    public final static String VERSION = "v1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Backendless.initApp(this, APP_ID, SECRET_KEY, VERSION);
        if(Backendless.UserService.loggedInUser()=="") {
            MainMenuFragment mainMenu = new MainMenuFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container,mainMenu).commit();
        } else{
            LoggedInFragment loggedInFragment = new LoggedInFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container,loggedInFragment).commit();
        }
    }
}