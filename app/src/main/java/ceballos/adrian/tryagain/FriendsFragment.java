package ceballos.adrian.tryagain;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.backendless.Backendless;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class FriendsFragment extends Fragment {
    private ArrayList<String> friends;
    private ArrayAdapter<String> friendsListAdapter;

    public FriendsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_friends2, container, false);
        Button addFriends = (Button) view.findViewById(R.id.addFriends);
        addFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setMessage("Add Friend");
                final EditText inputField = new EditText(getActivity());
                alertDialog.setView(inputField);
                alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertDialog.setPositiveButton("Add Friend", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        sendFriendRequest(inputField.getText().toString());
                        Toast.makeText(getActivity(), "Added a friend", Toast.LENGTH_SHORT).show();
                    }
                });
                alertDialog.create();
                alertDialog.show();
                                          }
        });

        friends = new ArrayList<>();
        friendsListAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,friends);
        ListView friendList = (ListView) view.findViewById(R.id.friendsList);
        friendList.setAdapter(friendsListAdapter);
        String currentUser = Backendless.UserService.loggedInUser();
        Backendless.Persistence.of(BackendlessUser.class).findById(currentUser, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser user) {
            Object[] friendObjects = (Object[]) user.getProperty("friends");
                if (friendObjects.length > 0){
                    BackendlessUser[] friendArray = (BackendlessUser[]) friendObjects;
                    for(BackendlessUser friend: friendArray){
                        String name = friend.getProperty("name").toString();
                        friends.add(name);
                        friendsListAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void handleFault(BackendlessFault fault) {

            }
        });

        return view;
    }
    private void sendFriendRequest(final String friendName){
        String currentUserId = Backendless.UserService.loggedInUser();
        Backendless.Persistence.of(BackendlessUser.class).findById(currentUserId, new AsyncCallback<BackendlessUser>() {
            @Override
            public void handleResponse(BackendlessUser currUser) {
                Intent intent = new Intent(getActivity(), TryAgainService.class);
                intent.setAction(Constants.ACTION_SEND_FRIEND_REQUEST);
                intent.putExtra("fromUser", currUser.getProperty("name").toString());
                intent.putExtra("toUser",friendName);
                getActivity().startService(intent);

            }

            @Override
            public void handleFault(BackendlessFault fault) {

            }
        });


    }


}
