package ceballos.adrian.tryagain;

/**
 * Created by Student on 6/2/2016.
 */
public class FriendRequest {
    private String toUser;
    private String fromUser;
    private Boolean accepted;
    public FriendRequest(){
        toUser = "";
        fromUser = "";
        accepted  = false;

    }
    public String getToUSer(){
        return  toUser;
    }
    public void setToUser(String user){
        toUser = user;
    } public String getfromUSer(){
        return  fromUser;
    }
    public void setfromUser(String user){
        fromUser = user;
    }public boolean isAccepted(){
        return accepted;
    }public void setAccepted(boolean isAccepted){
        accepted = isAccepted;
    }

}
