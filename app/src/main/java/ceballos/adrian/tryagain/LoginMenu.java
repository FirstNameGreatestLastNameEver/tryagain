package ceballos.adrian.tryagain;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class LoginMenu extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        LoginMenuFragment loginMenuFragment = new LoginMenuFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container,loginMenuFragment).commit();
    }

}
