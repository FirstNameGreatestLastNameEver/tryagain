package ceballos.adrian.tryagain;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class TryAgainReceiver extends BroadcastReceiver {
    public TryAgainReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if(action.equals(Constants.BROADCAST_ADD_FRIEND_SUCCESS)){
            Toast.makeText(context,"Added Friend",Toast.LENGTH_SHORT).show();
        }else if(action.equals(Constants.BROADCAST_ADD_FRIEND_FAILURE)){
            Toast.makeText(context,"Failed",Toast.LENGTH_SHORT).show();
        }else if(action.equals(Constants.BROADCAST_FRIEND_REQUEST_FAILURE)) {
            Toast.makeText(context, "Failed Friend request", Toast.LENGTH_SHORT).show();
        }else if(action.equals(Constants.BROADCAST_FRIEND_REQUEST_SUCCESS)) {
            Toast.makeText(context, "Success! Friend request sent", Toast.LENGTH_SHORT).show();
        }
    }
}
