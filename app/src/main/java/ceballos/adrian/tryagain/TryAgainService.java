package ceballos.adrian.tryagain;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.util.Log;

import com.backendless.Backendless;
import com.backendless.BackendlessCollection;
import com.backendless.BackendlessUser;
import com.backendless.async.callback.AsyncCallback;
import com.backendless.exceptions.BackendlessFault;
import com.backendless.persistence.BackendlessDataQuery;

import java.util.List;
import java.util.Objects;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class TryAgainService extends IntentService {

    public TryAgainService() {
        super("TryAgainService");
    }




    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            String action = intent.getAction();
            if(action.equals(Constants.ACTION_ADD_FRIEND)){
                String firstUserName = intent.getStringExtra("firstUserName");
                String secondUserName = intent.getStringExtra("secondUserName");
                Log.i("TryAgainService", "Service adding friend First user:" + firstUserName + "Second user:" + secondUserName);
                addFriends(firstUserName,secondUserName);
            }else if (action.equals(Constants.ACTION_SEND_FRIEND_REQUEST)){
                String fromUser = intent.getStringExtra("fromUser");
                String toUser = intent.getStringExtra("toUser");
                Log.i("TryAgainService", "From:" + fromUser + "To:" + toUser);
                sendFriendRequest(toUser,fromUser);
            }

        }
    }
    private void sendFriendRequest(final String fromUser, final String toUser){
        final BackendlessDataQuery query = new BackendlessDataQuery();
        query.setWhereClause(String.format("name = '%s'",toUser));
        Backendless.Persistence.of(BackendlessUser.class).find(query, new AsyncCallback<BackendlessCollection<BackendlessUser>>() {

            @Override
            public void handleResponse(BackendlessCollection<BackendlessUser> response) {
                if (response.getData().size()==0){
                    broadcastAddFriendFailure();
                }else{
                    FriendRequest friendRequest = new FriendRequest();
                    friendRequest.setToUser(toUser);
                    friendRequest.setfromUser(fromUser);
                    friendRequest.setAccepted(false);
                    Backendless.Persistence.save(friendRequest, new AsyncCallback<FriendRequest>() {
                        @Override
                        public void handleResponse(FriendRequest response) {
                            broadcastAddFriendSuccess();

                        }

                        @Override
                        public void handleFault(BackendlessFault fault) {
                            broadcastAddFriendFailure();
                        }
                    });
                }
            }

            @Override
            public void handleFault(BackendlessFault fault) {
            broadcastAddFriendFailure();
            }
        });
    }
    private void addFriends(String firstUserName, String secondUserName){
        BackendlessDataQuery query = new BackendlessDataQuery();
        query.setWhereClause(String.format("name = '%s' or name = '%s'", firstUserName, secondUserName));
        Backendless.Persistence.of(BackendlessUser.class).find(query, new AsyncCallback<BackendlessCollection<BackendlessUser>>() {
            @Override
            public void handleResponse(BackendlessCollection<BackendlessUser> response) {
                List<BackendlessUser> users = response.getData();
                if(users.size() != 2){
                    broadcastAddFriendFailure();
                }else{
                    BackendlessUser user1 = users.get(0);
                    final BackendlessUser user2 = users.get(1);
                    updateFriendsList(user1, user2);
                    Backendless.UserService.update(user1, new AsyncCallback<BackendlessUser>() {
                        @Override
                        public void handleResponse(BackendlessUser user) {
                            updateFriendsList(user2, user);
                            Backendless.UserService.update(user2, new AsyncCallback<BackendlessUser>() {
                                @Override
                                public void handleResponse(BackendlessUser response) {
                                    broadcastAddFriendSuccess();
                                }

                                @Override
                                public void handleFault(BackendlessFault fault) {
                                    broadcastAddFriendFailure();
                                }
                            });
                        }

                        @Override
                        public void handleFault(BackendlessFault fault) {
                            broadcastAddFriendFailure();
                        }
                    });
                }
            }

            @Override
            public void handleFault(BackendlessFault fault) {
            broadcastAddFriendFailure();
            }
        });
    }
    private  void broadcastAddFriendSuccess(){
        Intent intent = new Intent(Constants.BROADCAST_ADD_FRIEND_SUCCESS);
        sendBroadcast(intent);
    }
    private void broadcastAddFriendFailure(){
        Intent intent = new Intent(Constants.BROADCAST_ADD_FRIEND_FAILURE);
        sendBroadcast(intent);
    }
    private  void broadcastFriendRequestSuccess(){
        Intent intent = new Intent(Constants.BROADCAST_FRIEND_REQUEST_SUCCESS);
        sendBroadcast(intent);
    }
    private  void broadcastFriendRequestFailure(){
        Intent intent = new Intent(Constants.BROADCAST_FRIEND_REQUEST_FAILURE);
        sendBroadcast(intent);
    }
    private void updateFriendsList (BackendlessUser user, BackendlessUser friend){
    BackendlessUser [] newFriends;
        Object[] currentFriendObjects = (Object[]) user.getProperty("friends");
        if(currentFriendObjects.length > 0 ){
            BackendlessUser [] currentFriends = (BackendlessUser[])currentFriendObjects;
            newFriends =new BackendlessUser [currentFriends.length+1];
            for (int i = 0 ; i < currentFriends.length ; i++){
                newFriends[i] = currentFriends[i];
            }
            newFriends[newFriends.length-1] = friend;
        }else{
            newFriends =new BackendlessUser [] {friend};
        }
        user.setProperty("friends",newFriends);
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */

}
